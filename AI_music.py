from mido import Message, MidiFile, MidiTrack
import os
from collections import defaultdict
import collections
import sys
import numpy as np
import tensorflow as tf
import random
import bisect

time_bucket = [0, 10, 30, 50, 60, 80, 90, 100, 110, 120, 150, 160, 180, 200, 210, 230, 240, 360, 420, 480, 600, 660, 690, 720, 810,
               840, 930, 960, 1080, 1140, 1200, 1350, 1440]

# Put time in the nearest bucket
def bucketTime(time):
    index = bisect.bisect_left(time_bucket, time)
    if index >= len(time_bucket):
        return time_bucket[len(time_bucket)-1]
    t = time_bucket[index]
    if time == t:
        return time

    prev_time = time_bucket[index-1]
    diff1 = time - prev_time
    diff2 = t - time
    if diff1 > diff2:
        return t
    else:
        return prev_time


def readMidiFile(num_files=400, dir="data"):
    raw_note_data = []
    raw_time_data = []
    note_data = []
    time_data = []
    prev_note = -1
    time = 0
    time_set = set()
    note_set = set()
    print("Begin reading midi files:")
    for index, midi_file in enumerate(os.listdir(dir)):
        print(" Reading track %d" % index)
        if index == num_files:
            break
        midi_path = os.path.join(dir, midi_file)
        mid = MidiFile(midi_path)
        for i, track in enumerate(mid.tracks):
            flag = 0
            for msg in track:
                if not msg.is_meta:
                    if msg.type == "program_change":
                        flag += 1
                    try:
                        if flag == 2:
                            break
                        # We will not take a note when velocity is 0
                        # Because it just means that the note will stop
                        # By not taking it we maybe can clear some noise
                        if msg.note == prev_note:
                            time += msg.time
                            time = bucketTime(time)

                            # Update the last time to this time
                            time_data[-1] = time
                        else:
                            time = msg.time
                            time = bucketTime(time)
                            note_data.append(msg.note)
                            time_data.append(time)
                            note_set.add(msg.note)
                        time_set.add(time)
                        prev_note = msg.note
                        raw_note_data.append(msg.note)
                        raw_time_data.append(msg.time)

                    except:
                        pass
    return note_data, time_data, note_set, time_set, raw_note_data, raw_time_data


# Look for some interesting features
def getFeature(note_data, time_data):
    dict_notes = defaultdict(lambda: 0)
    dict_time = defaultdict(lambda: 0)
    dict_channels = defaultdict(lambda: 0)
    for (note, time) in zip(note_data, time_data):
        dict_notes[note] += 1
        dict_time[time] += 1

    # Sort in order for some insights
    sorted_dict_notes = sorted(dict_notes.items(), key=lambda x: x[1], reverse=True)
    sorted_dict_time = sorted(dict_time.items(), key=lambda x: x[1], reverse=True)

    # Get 25 most common time and then sort it in ascending order
    sorted_time = sorted(sorted_dict_time[:25], key=lambda x: x[0])

    print("Most common time: ")
    print(sorted_time)
    print("Total notes: ")
    print(len(sorted_dict_notes))
    print("(Note, Frequency) :")
    print(sorted_dict_notes)
    print("Total time:")
    print(len(sorted_dict_time))
    print("(Time, Frequency")
    print(sorted_dict_time)


def hashNote():
    note_hash = {}
    for note in range(notes_size):
        for time in range(time_size):
            hash_number = note + notes_size*time
            note_hash[hash_number] = (note, time)
    return note_hash


def hashTime(time_set):
    time_hash = {}
    reversed_time_hash = {}
    for index, time in enumerate(time_set):
        time_hash[time] = index
        reversed_time_hash[index] = time
    return time_hash, reversed_time_hash


def getNoteDict(note_set):
    note_dict = {}
    reversed_note_dict = {}
    for index, note in enumerate(note_set):
        note_dict[note] = index
        reversed_note_dict[index] = note
    return note_dict, reversed_note_dict


def note2id(note, time):
    return note + notes_size * time


def id2note(id):
    return note_hash[id]

batch_size = 64
num_unrollings = 10


class BatchGenerator(object):
    def __init__(self, notes_data, time_data, batch_size, num_unrollings):
        self._notes = notes_data
        self._data_size = len(notes_data)
        self._time = time_data
        self._batch_size = batch_size
        self._num_unrollings = num_unrollings
        segment = self._data_size // batch_size
        self._cursor = [offset*segment for offset in range(batch_size)]
        self._last_batch = self._next_batch()

    def _next_batch(self):
        batch = np.zeros(shape=(self._batch_size), dtype=np.int32)
        for b in range(self._batch_size):
            batch[b] = note2id(note_dict[self._notes[self._cursor[b]]], time_hash[self._time[self._cursor[b]]])
            self._cursor[b] = (self._cursor[b] + 1) % self._data_size
        return batch

    def next(self):
        batches = [self._last_batch]
        for step in range(self._num_unrollings):
            batches.append(self._next_batch())
        self._last_batch = batches[-1]
        return batches


# Generate one-hot encoding label
def getLabel(batch):
    label = np.zeros(shape=[batch.shape[0], len(note_hash)], dtype=float)
    for index, hash_number in enumerate(batch):
        label[index, hash_number] = 1
    return label


def getBatchesLabel(batches):
    label_arr = []
    for datum in batches:
        label = np.zeros(shape=[batch_size, len(note_hash)], dtype=float)
        for index, hash_number in enumerate(datum):
            label[index, hash_number] = 1
        label_arr.append(label)
    return label_arr


def writeMidi(note_sequence, name="song.mid"):
    mid = MidiFile()
    track = MidiTrack()
    mid.tracks.append(track)
    for note_id in note_sequence:
        note, time_index = note_hash[note_id]
        track.append(Message("note_on", note=reversed_note_dict[note], velocity=64, time=reversed_time_hash[time_index]))
    mid.save(name)


def logprob(predictions, labels):
    """Log-probability of the true labels in a predicted batch."""
    predictions[predictions < 1e-10] = 1e-10
    return np.sum(np.multiply(labels, -np.log(predictions))) / labels.shape[0]


def sample_distribution(distribution):
    """Sample one element from a distribution assumed to be an array of normalized
    probabilities.
    """
    r = random.uniform(0, 1)
    s = 0
    for i in range(len(distribution)):
        s += distribution[i]
        if s >= r:
          return i
    return len(distribution) - 1


def sample(prediction):
    """Turn a (column) prediction into 1-hot encoded samples."""
    p = np.zeros(shape=[1, len(note_hash)], dtype=np.float)
    p[0, sample_distribution(prediction[0])] = 1.0
    return p


def random_distribution():
    """Generate a random column of probabilities."""
    b = np.random.random(size=[1, len(note_hash)])
    return b/np.sum(b, 1)


def feedNote(note):
    b = np.zeros(shape=[1, len(note_hash)], dtype=np.float)
    b[0, note] = 1
    return b


def characters(probabilities):
  """Turn a 1-hot encoding or a probability distribution over the possible
  characters back into its (most likely) character representation."""
  return [c for c in np.argmax(probabilities, 1)]


def getPrediction(prediction, prediction_length=1):
    if prediction_length == 1:
        p = np.zeros(shape=[1, len(note_hash)], dtype=np.float)
        p[0, np.argmax(prediction, 1)] = 1.0
        return p

    # Get the first largest, the second largest and so on
    pred_arr = []
    for i in range(prediction_length):
        p = np.zeros(shape=[1, len(note_hash)], dtype=np.float)
        p[0, np.argmax(prediction, 1)] = 1
        prob = prediction[0, np.argmax(prediction, 1)]
        pred_arr.append((p, prob[0]))
        prediction[0, np.argmax(prediction, 1)] = -sys.maxsize
    return pred_arr


def beamSearch(music_length, input, beam_length=3):
    index = 0
    # Array of the note sequence with maximum probabilities
    max_notes_seq = []
    while index < music_length:
        probs_queue = collections.deque()
        prediction_queue = collections.deque()
        notes_queue = collections.deque()
        prediction = sample_prediction.eval({sample_input: input})

        # Use log to prevent small number precision
        probs_queue.append(np.log10(prediction[0, np.argmax(prediction, 1)][0]))
        notes_queue.append(characters(getPrediction(prediction)))
        prediction_queue.append(prediction)
        step = 0

        # If beam_length is 0 we use random
        if beam_length == 0:
            beam_length = random.randint(1, 5)
        while step < beam_length - 1:
            queue_len = len(probs_queue)
            while queue_len != 0:
                current_pred = prediction_queue.popleft()
                current_notes = notes_queue.popleft()
                current_prob = probs_queue.popleft()
                beam_preds = getPrediction(current_pred, beam_length)
                for beam_pred in beam_preds:
                    p, prob = beam_pred
                    new_notes = current_notes + characters(p)
                    notes_queue.append(new_notes)

                    # Log(MN) = log(M) + log(N)
                    new_prob = current_prob + np.log10(prob)
                    probs_queue.append(new_prob)
                    new_pred = sample_prediction.eval({sample_input: p})
                    prediction_queue.append(new_pred)
                queue_len -= 1
            step += 1

        # Get the max probability sequence
        max_prob = -sys.maxsize
        seq = None

        while len(probs_queue) != 0:
            prob = probs_queue.pop()
            seq_pop = notes_queue.pop()
            p = prediction_queue.pop()
            if prob > max_prob:
                max_prob = prob
                seq = seq_pop

                # Make it as a next input
                input = p

        # Concatenate with the previous note sequence
        max_notes_seq = max_notes_seq + seq
        index += beam_length
    return max_notes_seq

notes_data, time_data, note_set, time_set, raw_note_data, raw_time_data = readMidiFile()
notes_size = len(note_set)
time_size = len(time_set)
getFeature(raw_note_data, raw_time_data)
note_hash = hashNote()
time_hash, reversed_time_hash = hashTime(time_set)
#print(time_hash)
note_dict, reversed_note_dict = getNoteDict(note_set)
#print(note_dict)

valid_size = 100
valid_note = notes_data[:valid_size]
valid_time = time_data[:valid_size]
train_note = notes_data[valid_size:]
train_time = time_data[valid_size:]
train_batches = BatchGenerator(train_note, train_time, batch_size, num_unrollings)
valid_batches = BatchGenerator(valid_note, valid_time, 1, 1)
embedding_size = 128
num_nodes = 64


graph = tf.Graph()
with graph.as_default():
    # Parameters:
    # Embedding note
    note_embeddings = tf.Variable(
        tf.random_uniform([len(note_hash), embedding_size], -1.0, 1.0))
    # Input gate: input, previous output, and bias.
    ix = tf.Variable(tf.truncated_normal([embedding_size, num_nodes], -0.1, 0.1))
    im = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    ib = tf.Variable(tf.zeros([1, num_nodes]))

    # Forget gate: input, previous output, and bias.
    fx = tf.Variable(tf.truncated_normal([embedding_size, num_nodes], -0.1, 0.1))
    fm = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    fb = tf.Variable(tf.zeros([1, num_nodes]))

    # Memory cell: input, state and bias.
    cx = tf.Variable(tf.truncated_normal([embedding_size, num_nodes], -0.1, 0.1))
    cm = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    cb = tf.Variable(tf.zeros([1, num_nodes]))

    # Output gate: input, previous output, and bias.
    ox = tf.Variable(tf.truncated_normal([embedding_size, num_nodes], -0.1, 0.1))
    om = tf.Variable(tf.truncated_normal([num_nodes, num_nodes], -0.1, 0.1))
    ob = tf.Variable(tf.zeros([1, num_nodes]))

    # Variables saving state across unrollings.
    saved_output = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    saved_state = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)

    # Classifier weights and biases.
    w = tf.Variable(tf.truncated_normal([num_nodes, len(note_hash)], -0.1, 0.1))
    b = tf.Variable(tf.zeros([len(note_hash)]))

    # Definition of the cell computation.
    def lstm_cell(i, o, state):
        """Create a LSTM cell. See e.g.: http://arxiv.org/pdf/1402.1128v1.pdf
        Note that in this formulation, we omit the various connections between the
        previous state and the gates."""
        input_gate = tf.sigmoid(tf.matmul(i, ix) + tf.matmul(o, im) + ib)
        forget_gate = tf.sigmoid(tf.matmul(i, fx) + tf.matmul(o, fm) + fb)
        update = tf.matmul(i, cx) + tf.matmul(o, cm) + cb
        state = forget_gate * state + input_gate * tf.tanh(update)
        output_gate = tf.sigmoid(tf.matmul(i, ox) + tf.matmul(o, om) + ob)
        return output_gate * tf.tanh(state), state

    # Input data.
    train_data = list()
    train_label = list()
    for _ in range(num_unrollings):
        train_data.append(
            tf.placeholder(tf.int32, shape=[batch_size]))
        train_label.append(
            tf.placeholder(tf.float32, shape=[batch_size, len(note_hash)]))

    # Unrolled LSTM loop.
    outputs = list()
    output = saved_output
    state = saved_state
    for i in train_data:
        input_embed = tf.nn.embedding_lookup(note_embeddings, i)

        # Apply dropout here
        input_dropout = tf.nn.dropout(input_embed, 1)
        output, state = lstm_cell(input_dropout, output, state)
        output_dropout = tf.nn.dropout(output, 1)
        outputs.append(output_dropout)

    # State saving across unrollings.
    with tf.control_dependencies([saved_output.assign(output),
                                saved_state.assign(state)]):
        # Classifier.
        logits = tf.nn.xw_plus_b(tf.concat(outputs, 0), w, b)
        loss = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits(
                    labels=tf.concat(train_label, 0), logits=logits))

    # Optimizer.
    global_step = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(
        10.0, global_step, 500, 0.96, staircase=False)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    gradients, v = zip(*optimizer.compute_gradients(loss))
    gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
    optimizer = optimizer.apply_gradients(
    zip(gradients, v), global_step=global_step)

    # Predictions.
    train_prediction = tf.nn.softmax(logits)

    # Sampling and validation eval: batch 1, no unrolling.
    sample_input = tf.placeholder(tf.float32, shape=[1, len(note_hash)])
    sample_index = tf.argmax(sample_input, dimension=1)
    sample_input_embedding = tf.nn.embedding_lookup(note_embeddings, sample_index)
    saved_sample_output = tf.Variable(tf.zeros([1, num_nodes]))
    saved_sample_state = tf.Variable(tf.zeros([1, num_nodes]))
    reset_sample_state = tf.group(
        saved_sample_output.assign(tf.zeros([1, num_nodes])),
        saved_sample_state.assign(tf.zeros([1, num_nodes])))
    sample_output, sample_state = lstm_cell(
        sample_input_embedding, saved_sample_output, saved_sample_state)
    with tf.control_dependencies([saved_sample_output.assign(sample_output),
                                    saved_sample_state.assign(sample_state)]):
        sample_prediction = tf.nn.softmax(tf.nn.xw_plus_b(sample_output, w, b))

num_steps = 100001
summary_frequency = 10000
music_length = 400
output_path = "Outputs"

if not os.path.exists(output_path):
    os.mkdir(output_path)

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    mean_loss = 0
    for step in range(num_steps):
        batches = train_batches.next()
        feed_dict = dict()
        for i in range(num_unrollings):
            feed_dict[train_data[i]] = batches[i]
            feed_dict[train_label[i]] = getLabel(batches[i+1])
        _, l, predictions, lr = session.run(
            [optimizer, loss, train_prediction, learning_rate], feed_dict=feed_dict)
        mean_loss += l
        if step % summary_frequency == 0:
            if step > 0:
                mean_loss /= summary_frequency

            # The mean loss is an estimate of the loss over the last few batches.
            print('Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
            mean_loss = 0
            labels = getBatchesLabel(batches[1:])
            labels = np.concatenate(labels)
            print('Minibatch perplexity: %.2f' % float(
                np.exp(logprob(predictions, labels))))
            if step % (summary_frequency) == 0:
                # Generate some samples.
                print('=' * 80)
                for song_index in range(1):
                    # feed note using this formula note_hash = note + notes_size*time
                    # with notes_size = 72 and time is a index in time_bucket
                    feed = getPrediction(feedNote(1164))
                    reset_sample_state.run()
                    sentence = beamSearch(music_length, feed, 0)
                    sentence = characters(feed) + sentence
                    print(sentence)
                    print("Begin writing song")
                    song_name = output_path + "/" + "song{}-{}.mid".format(int(step/1000), song_index)
                    writeMidi(sentence, song_name)
                print('=' * 80)

            # Measure validation set perplexity.
            reset_sample_state.run()
            valid_logprob = 0
            for _ in range(valid_size):
                b = valid_batches.next()
                valid_input = getLabel(b[0])
                valid_label = getLabel(b[1])
                predictions = sample_prediction.eval({sample_input: valid_input})
                valid_logprob = valid_logprob + logprob(predictions, valid_label)
            print('Validation set perplexity: %.2f' % float(np.exp(valid_logprob / valid_size)))
