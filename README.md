## Artificial Intelligence Composer ##

### Getting started ###
* The program uses **LSTM** model to learn time series patterns
* Using over [300 classical songs](http://www.piano-midi.de/) as data

### Prerequisites ###
* Python 3.6 and pip

### Requirements ###
* Install Tensorflow framework by following this [link](https://www.tensorflow.org/install/)
* Pip install packages such as: numpy, scipy, mido.

### Run the code ###
* Open your command line and simply type:

	python AI_music.py

### Results ###
* Output director containing pieces of music over each epoch.
* Example pieces are in AI_song directory.